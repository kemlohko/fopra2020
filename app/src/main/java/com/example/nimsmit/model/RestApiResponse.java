package com.example.nimsmit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * This class represent the api response
 */
public class RestApiResponse {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private User user;

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("error")
    @Expose
    private String error;

    @SerializedName("records")
    @Expose
    private List<Travel> travels;


    public String getMessage() {
        return message;
    }

    public User getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }

    public String getError() {
        return error;
    }

    public List<Travel> getTravels() {
        return travels;
    }
}
