package com.example.nimsmit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * This class represent a travel.
 */
public class Travel implements Serializable {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("userID")
    @Expose
    private int userID;

    @SerializedName("departure")
    @Expose
    private String departure;

    @SerializedName("arrival")
    @Expose
    private String arrival;

    @SerializedName("created")
    @Expose
    private String created;

    @SerializedName("departure_date")
    @Expose
    private String departure_date;

    @SerializedName("departure_hour")
    @Expose
    private String departure_hour;

    @SerializedName("arrival_date")
    @Expose
    private String arrival_date;

    @SerializedName("arrival_hour")
    @Expose
    private String arrival_hour;

    @SerializedName("weight")
    @Expose
    private double weight;

    @SerializedName("price")
    @Expose
    private double price;

    @SerializedName("commentar")
    @Expose
    private String commentar;



    public Travel(int userID, String departure, String destination_city, String departure_date, String departure_hour, String arrival_date, String arrival_hour, double weight, double price, String commentar) {
        this.userID = userID;
        this.departure = departure;
        this.arrival = destination_city;
        this.departure_date = departure_date;
        this.departure_hour =departure_hour;
        this.arrival_date = arrival_date;
        this.arrival_hour = arrival_hour;
        this.weight = weight;
        this.price = price;
        this.commentar = commentar;
    }

    public Travel(int id, int userID, String departure, String arrival, String departure_date, String departure_hour, String arrival_date, String arrival_hour, double weight, double price, String commentar) {
        this.id = id;
        this.userID = userID;
        this.departure = departure;
        this.arrival = arrival;
        this.created = created;
        this.departure_date = departure_date;
        this.departure_hour = departure_hour;
        this.arrival_date = arrival_date;
        this.arrival_hour = arrival_hour;
        this.weight = weight;
        this.price = price;
        this.commentar = commentar;
    }



    public int getId() {
        return id;
    }

    public int getUserID() {
        return userID;
    }

    public String getDeparture() {
        return departure;
    }

    public String getArrival() {
        return arrival;
    }

    public String getCreated() {
        return created;
    }

    public String getDeparture_date() {
        return departure_date;
    }

    public String getArrival_date() {
        return arrival_date;
    }

    public double getWeight() {
        return weight;
    }

    public double getPrice() {
        return price;
    }

    public String getCommentar() {
        return commentar;
    }

    public String getDeparture_hour() {
        return departure_hour;
    }

    public String getArrival_hour() {
        return arrival_hour;
    }
}
