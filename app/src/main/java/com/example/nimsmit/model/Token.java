package com.example.nimsmit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * This class represent a token.
 */
public class Token {

    @SerializedName("token")
    @Expose
    private String token;

    public Token(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
