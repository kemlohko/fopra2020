package com.example.nimsmit.model;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;
/**
 * this class is used to create and setup time dialog.
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    private String tag;
    private  TimeListener timeListener;

    public TimePickerFragment(String tag) {
        this.tag = tag;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        return new TimePickerDialog(requireContext(), this, hour, minute, android.text.format.DateFormat.is24HourFormat(requireContext()));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String time = hourOfDay + ":" + minute;
        timeListener.setTime(time, tag);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        timeListener = (TimeListener) context;
    }

    /**
     * This Interface must been implemented by the activity who which display the date dialog
     */
    public interface TimeListener{
        void setTime(String time, String tag);
    }
}
