package com.example.nimsmit.model;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.text.DateFormat;
import java.util.Calendar;

/**
 * this class is used to create and setup date dialog.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private String tag;
    private DateInterface dateInterface;

    public DatePickerFragment(String tag) {
        this.tag = tag;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(requireActivity(), this, year, month, day);
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String currentDateString = DateFormat.getDateInstance(DateFormat.MEDIUM).format(cal.getTime());
        String date = year + "-" + month +"-" + dayOfMonth;
        dateInterface.setDate(date, tag);
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        dateInterface = (DateInterface) context;
    }

    /**
     * This Interface must been implemented by the activity who which display the date dialog
     */
    public interface DateInterface{
        void setDate(String date, String tag);
    }


}
