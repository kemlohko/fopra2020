package com.example.nimsmit.model;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nimsmit.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This class represent a adapter for the recyclerview
 */
public class RVAdapter extends RecyclerView.Adapter<RVAdapter.TravelViewHolder> implements Filterable {

    private List<Travel> travelsList;
    private List<Travel> filteredTravelsList;
    private TravelAdapterListener travelAdapterListener;

    public RVAdapter(List<Travel> travelsList, TravelAdapterListener travelAdapterListener) {
        this.travelsList = travelsList;
        this.filteredTravelsList = travelsList;
        this.travelAdapterListener = travelAdapterListener;
    }



    @NonNull
    @Override
    public TravelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.travel_item1, parent, false);
        return new TravelViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final TravelViewHolder holder, int position) {
        Travel travel = filteredTravelsList.get(position);
        int userID = travel.getUserID();

        ApiInterface apiInterface = ApiClient.getClient();
        Call<User> call = apiInterface.get_user(userID);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    String avatar_link = response.body().getAvatar();
                    Helper.loadImage(avatar_link, holder.travel_creator_image, null, null);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
        holder.departure.setText(travel.getDeparture());
        holder.departure_date.setText(travel.getDeparture_date());
        holder.departure_hour.setText(travel.getDeparture_hour());
        holder.arrival.setText(travel.getArrival());
        holder.arrival_date.setText(travel.getArrival_date());
        holder.arrival_hour.setText(travel.getArrival_hour());

    }

    @Override
    public int getItemCount() {
        return filteredTravelsList.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // filter method
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (charString.isEmpty()){
                    filteredTravelsList = travelsList;
                }else {
                    List<Travel> filteredList = new ArrayList<>();
                    for (Travel travel : travelsList){

                        if (travel.getDeparture().toLowerCase().contains(charString.toLowerCase()) || travel.getArrival().toLowerCase().contains(charString.toLowerCase())){
                            filteredList.add(travel);
                        }
                    }
                    filteredTravelsList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredTravelsList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredTravelsList = (ArrayList) results.values;
                notifyDataSetChanged();
            }
        };
    }


    /**
     * create a holder model for each travel
     */
    public class TravelViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView departure, arrival, departure_date, departure_hour, arrival_date, arrival_hour;
        ImageView travel_creator_image;

        public TravelViewHolder(@NonNull final View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
            departure = itemView.findViewById(R.id.show_travel_departure);
            arrival = itemView.findViewById(R.id.show_travel_destination);
            departure_date = itemView.findViewById(R.id.show_travel_departure_date);
            departure_hour = itemView.findViewById(R.id.show_travel_departure_hour);
            arrival_date = itemView.findViewById(R.id.show_travel_arrival_date);
            arrival_hour = itemView.findViewById(R.id.show_travel_arrival_hour);
            travel_creator_image =itemView.findViewById(R.id.show_travel_creator_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    travelAdapterListener.onTravelSelected(filteredTravelsList.get(getAdapterPosition()));
                }
            });
        }
    }

    /**
     * This interface must been implemented by the fragment that display travels
     */
    public interface TravelAdapterListener{
        void onTravelSelected(Travel travel);
    }
}
