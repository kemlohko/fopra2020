package com.example.nimsmit.model;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * This interface define all HTTP Methods to communicate with the API
 */
public interface ApiInterface {

    @POST("login_user.php")
    Call<RestApiResponse> login(
            @Body Login login
    );

    @POST("validate_token.php")
    Call<RestApiResponse> validate_token(
            @Body Token token
    );

    @POST("register_user.php")
    Call<RestApiResponse> register_user(
            @Body User user
    );

    @POST("delete_user.php")
    Call<RestApiResponse> delete_account(
            @Query("id") int id
    );

    @POST("delete_travel.php")
    Call<RestApiResponse> delete_travel(
            @Query("id") int id
    );

    @POST("history.php")
    Call<RestApiResponse> history(
            @Query("id") int id
    );

    @GET("read_all.php")
    Call<RestApiResponse> read_all_travels();

    @POST("search_travel.php")
    Call<RestApiResponse> search_travel(
            @Body SearchTravel searchTravel
    );


    @POST("get_user.php")
    Call<User> get_user(
            @Query("id") int id
    );

    @POST("create_travel.php")
    Call<RestApiResponse> create_travel(
            @Body Travel travel
    );

    @Multipart
    @POST("update_user.php")
    Call<RestApiResponse> update_user(
            @Part("user_json") RequestBody user,
            @Part MultipartBody.Part profile_image
    );

    @Multipart
    @POST("update_user.php")
    Call<RestApiResponse> update(
            @Part("user_json") RequestBody user
    );

    @Multipart
    @POST("upload_foto.php")
    Call<RestApiResponse> update_foto(
            @Part MultipartBody.Part image

    );


}
