package com.example.nimsmit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * this class represent a search travel object.
 */
public class SearchTravel implements Serializable {

    @SerializedName("departure")
    @Expose
    private String departure;

    @SerializedName("destination")
    @Expose
    private String destination;

    public SearchTravel(String departure, String destination) {
        this.departure = departure;
        this.destination = destination;
    }

    public String getDeparture() {
        return departure;
    }

    public String getDestination() {
        return destination;
    }

}
