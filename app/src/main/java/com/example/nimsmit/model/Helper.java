package com.example.nimsmit.model;

import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This class provide some useful methods
 */
public class Helper {

    /**
     * check if the view is empty or not
     * @param editText the view to check
     * @return true or false
     */
    public static Boolean isEmpty(EditText editText){

        String string = editText.getText().toString();
        if (TextUtils.isEmpty(string)){
            editText.setError("This field can't be empty");
            return true;
        }
        return false;
    }

    /**
     * check if the view is empty or not
     * @param textView the view to check
     * @return true or false
     */
    public static Boolean isEmpty(TextView textView){

        String string = textView.getText().toString();
        if (TextUtils.isEmpty(string)){
            textView.setError("This field can't be empty");
            return true;
        }
        return false;
    }

    /**
     * load a image from an url. This method use the picasso library
     * @param link the url to load from
     * @param imageView the image view that will hold the downloaded image
     * @param width the require width
     * @param height the require height
     */
    public static void loadImage(String link, ImageView imageView, Integer width, Integer height){
        if (width != null && height != null){
            Picasso.get()
                    .load(link)
                    .resize(width, height)
                    .into(imageView);
        }
        else {
            Picasso.get()
                    .load(link)
                    .into(imageView);

        }

    }


}
