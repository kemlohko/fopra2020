package com.example.nimsmit.model;

import android.graphics.Bitmap;

/**
 * This interface define some functions that are used to pass data between fragment or activity
 */
public interface DoSomething {

    /**
     * change the user profil image
     * @param image the new image
     */
    void change_profile_image(Bitmap image);

    /**
     *
     * @return an User object
     */
    User getUser();

    /**
     * check if the user is logged
     * @return true or false
     */
    boolean logged();

    /**
     * change the user lastname
     * @param newFirstname the new lastname
     */
    void userNewLastname(String newFirstname);

}
