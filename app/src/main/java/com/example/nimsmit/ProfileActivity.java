package com.example.nimsmit;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;


import com.example.nimsmit.model.ApiClient;
import com.example.nimsmit.model.ApiInterface;
import com.example.nimsmit.model.DatePickerFragment;
import com.example.nimsmit.model.DoSomething;
import com.example.nimsmit.model.Helper;
import com.example.nimsmit.model.RestApiResponse;
import com.example.nimsmit.model.TimePickerFragment;
import com.example.nimsmit.model.User;
import com.google.android.material.navigation.NavigationView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

/**
 * this activity manage all the action that occur when the user is logged
 */

public class ProfileActivity extends AppCompatActivity implements DatePickerFragment.DateInterface, TimePickerFragment.TimeListener, DoSomething {

    private AppBarConfiguration mAppBarConfiguration;
    private DrawerLayout drawer;
    private ImageView profile_image;
    private ApiInterface apiInterface;
    private int userID;
    private User user_date;
    String lastname;
    String avatar_link;
    String salutation = "Hallo ";
    TextView nav_salutation;
    View headerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();

    }

    /**
     * this method initialise the view with all the components
     */
    private void init() {

        Toolbar toolbar = findViewById(R.id.toolbar_profile);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout_profile);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().clear();
        navigationView.inflateMenu(R.menu.activity_profile_drawer);


        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_user_add_travel, R.id.nav_user_search_travel, R.id.nav_user_history, R.id.nav_user_update)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_user_logged);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        // inflate navigationview
        // we remove    app:headerLayout="@layout/nav_header_profile" from xml layout otherwise it will create duplicate
        headerView = navigationView.inflateHeaderView(R.layout.nav_header_profile);
        nav_salutation = headerView.findViewById(R.id.nav_header_salutation);

        Intent intent = getIntent();
        user_date = (User) intent.getSerializableExtra("data_user");
        assert user_date != null;
        userID = user_date.getUserID();
        // set profile image
        profile_image = headerView.findViewById(R.id.nav_header_profile_image);
        avatar_link = user_date.getAvatar();
        Helper.loadImage(avatar_link, profile_image, 400, 350);
        // set salutation
        lastname = user_date.getLastname();
        salutation = salutation + lastname;
        nav_salutation.setText(salutation);
        apiInterface = ApiClient.getClient();

    }


    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_user_logged);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_logout:
                backToHomeActivity();
                return true;
            case R.id.action_delete_account:
                confirmDeleteAccountAlert();
            default:
                return false;
        }

    }


    /**
     * this method create an AlertDialog to confirm that user really want to delete his account.
     */
    private void confirmDeleteAccountAlert() {

        new AlertDialog.Builder(ProfileActivity.this)
                .setTitle("Delete Account")
                .setMessage("Do you really want to delete your account?")
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteAccount();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }


    /**
     * this method perform a deletion of an user account and take the user back to home activity
     */
    private void deleteAccount() {

        Call<RestApiResponse> call = apiInterface.delete_account(userID);
        call.enqueue(new Callback<RestApiResponse>() {
            @EverythingIsNonNull
            @Override
            public void onResponse(Call<RestApiResponse> call, Response<RestApiResponse> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Toast.makeText(ProfileActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    backToHomeActivity();
                }
            }

            @Override
            public void onFailure(Call<RestApiResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }


    /**
     * this method take the user from ProfileActivity back to MainActivity
     */
    private void backToHomeActivity() {
        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }


    /**
     * this method set the date when user create a new travel
     * @param date the date that user selected
     * @param tag help to differentiate between departure date and arrival date
     */
    @Override
    public void setDate(String date, String tag) {
        if (tag.equals("datePicker1")){
            TextView departure_date = findViewById(R.id.add_travel_departure_date);
            departure_date.setText(date);
        }
        if (tag.equals("datePicker2")){
            TextView arrival_date = findViewById(R.id.add_travel_arrival_date);
            arrival_date.setText(date);
        }


    }


    /**
     * this method set the time when user create a new travel
     * @param time the time the user selected
     * @param tag help to differentiate between departure time and arrival time
     */
    @Override
    public void setTime(String time, String tag) {
        if (tag.equals("timePicker1")){
            TextView departure_time = findViewById(R.id.add_travel_departure_hour);
            departure_time.setText(time);
        }
        if (tag.equals("timePicker2")){
            TextView arrival_time = findViewById(R.id.add_travel_arrival_hour);
            arrival_time.setText(time);
        }
    }


    /**
     * this methode change the profile image when user update his profile
     * @param image the image the user selected
     */
    @Override
    public void change_profile_image(Bitmap image) {
        profile_image.setImageBitmap(image);

    }


    /**
     * this method get the user data
     * @return user data
     */
    @Override
    public User getUser() {
        return user_date;
    }

    /**
     * this method return true if the user is logged
     * @return true
     */
    @Override
    public boolean logged() {
        return true;
    }

    /**
     * this method change the user lastname in salutation when the profile is updated
     * @param newFirstname the new lastname that user choose
     */
    @Override
    public void userNewLastname(String newFirstname){
        nav_salutation.setText(salutation + newFirstname);
    }


}

