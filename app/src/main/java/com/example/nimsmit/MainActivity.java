package com.example.nimsmit;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.nimsmit.model.DoSomething;
import com.example.nimsmit.model.User;
import com.example.nimsmit.ui.ShowSelectedTravelFragment;
import com.google.android.material.navigation.NavigationView;


/**
 * this class is the main activity
 */
public class MainActivity extends AppCompatActivity  implements DoSomething {

    private AppBarConfiguration mAppBarConfiguration;
    private DrawerLayout drawer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }


    /**
     * this method initialise the view with all the components
     */
    private void init() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Bundle bundle = new Bundle();
        bundle.putBoolean("logged", false);
        ShowSelectedTravelFragment showSelectedTravelFragment = new ShowSelectedTravelFragment();
        showSelectedTravelFragment.setArguments(bundle);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_login, R.id.nav_register)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

    }


    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }
    }

    /**
     * this method change the profile image when the user update his profile
     * @param image the image that user selected
     */
    @Override
    public void change_profile_image(Bitmap image) {

    }


    /**
     * this method return the user data
     * @return null
     */
    @Override
    public User getUser() {
        return null;
    }


    /**
     * this method return true if the user is logged
     * @return false
     */
    @Override
    public boolean logged() {
        return false;
    }


    /**
     * this method update the lastname in salutation when user update his profile
     * @param newFirstname the new lastname that user choose
     */
    @Override
    public void userNewLastname(String newFirstname) {

    }


}
