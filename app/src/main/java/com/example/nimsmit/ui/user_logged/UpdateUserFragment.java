package com.example.nimsmit.ui.user_logged;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.nimsmit.R;
import com.example.nimsmit.model.ApiClient;
import com.example.nimsmit.model.ApiInterface;
import com.example.nimsmit.model.DoSomething;
import com.example.nimsmit.model.FileUtils;
import com.example.nimsmit.model.RestApiResponse;
import com.example.nimsmit.model.User;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class UpdateUserFragment extends Fragment {

    private static final int STORAGE_PERMISSION_CODE = 100;
    private DoSomething doSomething;
    private EditText firstname, lastname, email, password;
    private ImageView imageView;
    private Button update, change_image;
    private final int REQUEST_CODE = 1;
    private Bitmap profile_image;
    private String token;
    private ApiInterface apiInterface;
    private Uri selectedImageUri;
    private User user_data;
    private User new_user_data;
    private ProgressBar progressBar;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_update_user, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        // get update user data from server
        int userID = user_data.getUserID();
        getUserData(userID);

        change_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new_user_data = getNewUserData();

                if (profile_image != null ) {
                    doSomething.change_profile_image(profile_image);
                    imageView.setImageBitmap(null);
                    // check for storage permission and upload user data if permission garanted
                    checkForPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, STORAGE_PERMISSION_CODE);
                }
                else {
                    uploadFile(new_user_data);
                }
                }

        });

    }

    private void init(View view) {
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        apiInterface = ApiClient.getClient();
        user_data = doSomething.getUser();
        firstname = view.findViewById(R.id.update_firstname);
        lastname = view.findViewById(R.id.update_lastname);
        email = view.findViewById(R.id.update_email);
        password = view.findViewById(R.id.update_password);
        update = view.findViewById(R.id.update_user);
        imageView =view.findViewById(R.id.update_profile_image);
        change_image = view.findViewById(R.id.update_change_image);
        token = user_data.getToken();
    }

    private User getNewUserData() {

        String newFirstname = firstname.getText().toString();
        String newLastname = lastname.getText().toString();
        String newEmail = email.getText().toString();
        String newPassword = password.getText().toString();
        doSomething.userNewLastname(newLastname);
        return new User(newFirstname, newLastname, newEmail, newPassword, token);
    }



    private void uploadFile(User new_user_data){

        String user_json = new Gson().toJson(new_user_data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), user_json);

        Call<RestApiResponse> call;
        if (selectedImageUri != null){
            File file = new File(FileUtils.getPath(requireContext(), selectedImageUri));
            RequestBody requestBodyFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", file.getName(), requestBodyFile);

            call = apiInterface.update_user(requestBody, filePart);
        }

        else{
            call = apiInterface.update_user(requestBody, null);
        }
        progressBar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<RestApiResponse>() {
            @Override
            public void onResponse(Call<RestApiResponse> call, Response<RestApiResponse> response) {
                progressBar.setVisibility(View.INVISIBLE);
                if (response.isSuccessful()){
                    Toast.makeText(requireContext(), "changes saved", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RestApiResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });


    }


    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "pick an image"), REQUEST_CODE);


    }


    private void getUserData(int userID){

        Call<User> call = apiInterface.get_user(userID);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){
                    User user = response.body();
                    assert user != null;
                    firstname.setText(user.getFirstname());
                    lastname.setText(user.getLastname());
                    email.setText(user.getEmail());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK && data != null){
            selectedImageUri = data.getData();
            Bitmap b = null;
            try {
                b = MediaStore.Images.Media.getBitmap(requireActivity().getContentResolver(), selectedImageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            profile_image = Bitmap.createScaledBitmap(b,250, 250, false);
            imageView.setImageBitmap(profile_image);
        }


    }


    private void checkForPermissions(String permission, int requestCode){

        if (ContextCompat.checkSelfPermission(requireContext(), permission)
                == PackageManager.PERMISSION_DENIED){

            // requesting the permission
            requestPermissions( new String[] { permission }, requestCode);
        }
        else {
            // permission is already garanted
            uploadFile(new_user_data);
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == STORAGE_PERMISSION_CODE){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                uploadFile(new_user_data);
            }

        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        doSomething = (DoSomething) context;
    }
}
