package com.example.nimsmit.ui.user_logged;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.nimsmit.R;
import com.example.nimsmit.model.ApiClient;
import com.example.nimsmit.model.ApiInterface;
import com.example.nimsmit.model.DatePickerFragment;
import com.example.nimsmit.model.DoSomething;
import com.example.nimsmit.model.Helper;
import com.example.nimsmit.model.RestApiResponse;
import com.example.nimsmit.model.TimePickerFragment;
import com.example.nimsmit.model.Travel;
import com.example.nimsmit.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;


public class AddTravelFragment extends Fragment {
    private Button create_travel;
    private EditText departure, arrival, weight, price, commentar;
    private TextView  departure_date, departure_time, arrival_date, arrival_time;
    private User user_data;
    private String tag;
    private ApiInterface apiInterface;
    private DoSomething doSomething;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_add_travel, container, false);
        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);

        departure_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tag = "datePicker1";
                selectDate(tag);

            }
        });

        arrival_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tag = "datePicker2";
                selectDate(tag);
            }
        });


        departure_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tag = "timePicker1";
                selectTime(tag);
            }
        });

        arrival_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tag = "timePicker2";
                selectTime(tag);

            }
        });

        create_travel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeTravel();
            }
        });

    }

    private void init(View view) {

        departure = view.findViewById(R.id.add_travel_departure);
        arrival = view.findViewById(R.id.add_travel_arrival);
        departure_date = view.findViewById(R.id.add_travel_departure_date);
        departure_time = view.findViewById(R.id.add_travel_departure_hour);
        arrival_date = view.findViewById(R.id.add_travel_arrival_date);
        arrival_time = view.findViewById(R.id.add_travel_arrival_hour);
        weight = view.findViewById(R.id.add_travel_weight);
        price = view.findViewById(R.id.add_travel_price);
        commentar = view.findViewById(R.id.add_travel_message);
        create_travel = view.findViewById(R.id.create_travel);
        user_data = doSomething.getUser();
        apiInterface = ApiClient.getClient();
    }


    private void makeTravel() {

        if(!Helper.isEmpty(departure) && !Helper.isEmpty(arrival) && !Helper.isEmpty(departure_date) &&
                !Helper.isEmpty(departure_time) && !Helper.isEmpty(arrival_date) && !Helper.isEmpty(arrival_time) &&
                !Helper.isEmpty(weight) && !Helper.isEmpty(price) ){

            final String depart = departure.getText().toString();
            String destination = arrival.getText().toString();
            String depart_date = departure_date.getText().toString();
            final String depart_time = departure_time.getText().toString();
            String destination_date = arrival_date.getText().toString();
            String destination_time = arrival_time.getText().toString();
            double travel_weight = Double.parseDouble(weight.getText().toString());
            double travel_price = Double.parseDouble(price.getText().toString());
            String message = commentar.getText().toString();
            int userID = user_data.getUserID();

            Travel newTravel = new Travel(userID, depart, destination, depart_date, depart_time,
                    destination_date, destination_time, travel_weight, travel_price, message);

            Call<RestApiResponse> call = apiInterface.create_travel(newTravel);
            call.enqueue(new Callback<RestApiResponse>() {
                @EverythingIsNonNull
                @Override
                public void onResponse(Call<RestApiResponse> call, Response<RestApiResponse> response) {
                    if (response.isSuccessful()){
                        assert response.body() != null;
                        String message = response.body().getMessage();
                        Toast.makeText(requireActivity(), message, Toast.LENGTH_SHORT).show();
                        // clear EditText
                        departure.getText().clear();
                        arrival.getText().clear();
                        weight.getText().clear();
                        price.getText().clear();
                        commentar.getText().clear();
                        departure_date.setText("");
                        departure_time.setText("");
                        arrival_date.setText("");
                        arrival_time.setText("");

                    }
                }

                @Override
                public void onFailure(Call<RestApiResponse> call, Throwable t) {
                    t.printStackTrace();

                }
            });
        }
    }


    private void selectTime(String tag) {
        DialogFragment timePicker = new TimePickerFragment(tag);
        timePicker.show(requireFragmentManager(), tag);
    }

    private void selectDate(String tag) {
        DialogFragment datePicker = new DatePickerFragment(tag);
        datePicker.show(requireFragmentManager(), tag);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        doSomething = (DoSomething) context;
    }
}
