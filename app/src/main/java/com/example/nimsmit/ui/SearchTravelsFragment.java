package com.example.nimsmit.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.nimsmit.R;
import com.example.nimsmit.model.Helper;
import com.example.nimsmit.model.SearchTravel;

/**
 *
 */
public class SearchTravelsFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_search_travels, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {

        Button search_travel = view.findViewById(R.id.search_travel);
        Button show_all_travel = view.findViewById(R.id.show_all_travels);
        final EditText departure = view.findViewById(R.id.search_travel_departure);
        final EditText arrival = view.findViewById(R.id.search_travel_destination);
        final Bundle bundle = new Bundle();

        search_travel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Helper.isEmpty(departure) && !Helper.isEmpty(arrival)){
                    String depart = departure.getText().toString();
                    String destination = arrival.getText().toString();
                    SearchTravel searchTravel = new SearchTravel(depart, destination);
                    bundle.putSerializable("search_travel", searchTravel);
                    NavHostFragment.findNavController(SearchTravelsFragment.this)
                            .navigate(R.id.action_HomeFragment_to_ShowTravelFragment, bundle);
                }

            }
        });

        show_all_travel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NavHostFragment.findNavController(SearchTravelsFragment.this)
                       .navigate(R.id.action_HomeFragment_to_ShowTravelFragment, bundle);

            }
        });
    }


}
