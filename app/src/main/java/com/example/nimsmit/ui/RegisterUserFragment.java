package com.example.nimsmit.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.nimsmit.ProfileActivity;
import com.example.nimsmit.R;
import com.example.nimsmit.model.ApiClient;
import com.example.nimsmit.model.ApiInterface;
import com.example.nimsmit.model.Helper;
import com.example.nimsmit.model.RestApiResponse;
import com.example.nimsmit.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This class is the register fragment and hold methods to register an user.
 */
public class RegisterUserFragment extends Fragment {

    private EditText firstname, lastname, email, password;
    private Button register;
    private ApiInterface apiInterface;
    private User user;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_register_user, container, false);
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firstname = view.findViewById(R.id.register_firstname);
        lastname = view.findViewById(R.id.register_lastname);
        email = view.findViewById(R.id.register_email);
        password = view.findViewById(R.id.register_password);
        register = view.findViewById(R.id.register);

        //  api interface
        apiInterface = ApiClient.getClient();
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

    }

    /**
     * method to register the user.
     */
    private void register() {

        if (!Helper.isEmpty(firstname) && !Helper.isEmpty(lastname) && !Helper.isEmpty(email) && !Helper.isEmpty(password)){
            String first_name = firstname.getText().toString();
            String last_name = lastname.getText().toString();
            String user_email = email.getText().toString();
            String user_password = password.getText().toString();
            user = new User(first_name, last_name, user_email, user_password);

            Call<RestApiResponse> call = apiInterface.register_user(user);
            call.enqueue(new Callback<RestApiResponse>() {
                @Override
                public void onResponse(Call<RestApiResponse> call, Response<RestApiResponse> response) {
                    if (response.isSuccessful()){
                        assert response.body() != null;
                        profile(response.body().getUser());
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getActivity(), "Email empty or already exists", Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<RestApiResponse> call, Throwable t) {

                }
            });
        }


    }

    /**
     * navigate to the profil activity.
     * @param data the user data.
     */
    private void profile(User data) {
        Intent profile = new Intent(getActivity(), ProfileActivity.class);
        profile.putExtra("data_user", data);
        startActivity(profile);
        requireActivity().finish();

    }



}
