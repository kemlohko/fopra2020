package com.example.nimsmit.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.nimsmit.R;
import com.example.nimsmit.model.ApiClient;
import com.example.nimsmit.model.ApiInterface;
import com.example.nimsmit.model.DoSomething;
import com.example.nimsmit.model.Helper;
import com.example.nimsmit.model.RestApiResponse;
import com.example.nimsmit.model.Travel;
import com.example.nimsmit.model.User;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

import static java.lang.String.valueOf;

public class ShowSelectedTravelFragment extends Fragment {

    private ApiInterface apiInterface;
    private Travel selectedTravel;
    private String firstname, lastname, email, avatar_link;
    private ImageView poster_image;
    private TextView poster_firstname,poster_lastname, poster_residence, poster_experience;
    private TextView departure, departure_date, departure_time;
    private TextView arrival, arrival_date, arrival_time;
    private TextView weight, price, commentar;
    private int userID;
    private FloatingActionButton floatingActionButton;
    private boolean delete;
    private DoSomething doSomething;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        View root = inflater.inflate(R.layout.fragment_show_selected_travel, container, false);
        init(root);
        // change the floating button image ressource if the user is in History fragment and want to delete a travel
        assert getArguments() != null;
        delete = getArguments().getBoolean("delete");
        if (delete){
            floatingActionButton.setImageResource(R.drawable.ic_action_delete_forever);
        }
        return root;
    }

    private void init(View root) {

        setHasOptionsMenu(true);
        departure = root.findViewById(R.id.travel_departure);
        departure_date = root.findViewById(R.id.travel_departure_date);
        departure_time = root.findViewById(R.id.travel_departure_hour);
        arrival = root.findViewById(R.id.travel_arrival);
        arrival_date = root.findViewById(R.id.travel_arrival_date);
        arrival_time = root.findViewById(R.id.travel_arrival_hour);
        weight = root.findViewById(R.id.travel_weight);
        price = root.findViewById(R.id.travel_price);
        commentar = root.findViewById(R.id.travel_commentar);
        poster_firstname = root.findViewById(R.id.poster_firstname);
        poster_lastname = root.findViewById(R.id.poster_lastname);
        poster_image = root.findViewById(R.id.poster_image);

        floatingActionButton = root.findViewById(R.id.floatingButton);

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        assert getArguments() != null;
        selectedTravel = (Travel) getArguments().getSerializable("travel");
        assert selectedTravel != null;
        userID = selectedTravel.getUserID();
        departure.setText(selectedTravel.getDeparture());
        departure_date.setText(selectedTravel.getDeparture_date());
        departure_time.setText(selectedTravel.getDeparture_hour());
        arrival.setText(selectedTravel.getArrival());
        arrival_date.setText(selectedTravel.getArrival_date());
        arrival_time.setText(selectedTravel.getArrival_hour());
        String kg = selectedTravel.getWeight() + " kg";
        weight.setText(kg);
        price.setText(valueOf(selectedTravel.getPrice()));
        commentar.setText(selectedTravel.getCommentar());
        apiInterface = ApiClient.getClient();

        getData();
        final boolean logged = doSomething.logged();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!logged){
                    Toast.makeText(getContext(), "please sign in", Toast.LENGTH_LONG).show();
                }
                if (logged && !delete){
                    sendEmail();
                }
                if (logged && delete){
                    // delete travel
                    int travelID = selectedTravel.getId();
                    confirmDeleteAlert(travelID);
                }


            }
        });


    }

    // this method get the user that create the travel
    private void getData() {

        Call<User> call = apiInterface.get_user(userID);
        call.enqueue(new Callback<User>() {
            @EverythingIsNonNull
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    firstname = response.body().getFirstname();
                    lastname = response.body().getLastname();
                    email = response.body().getEmail();
                    avatar_link = response.body().getAvatar();
                    poster_firstname.setText(firstname);
                    poster_lastname.setText(lastname);
                    Helper.loadImage(avatar_link, poster_image, null, null);

                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void sendEmail() {

        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        String subject = "Interesse an der Reise von " + selectedTravel.getDeparture() + " nach " +
                selectedTravel.getArrival() + " am " + selectedTravel.getArrival_date() + " um " +
                selectedTravel.getDeparture_hour();

        emailIntent.setDataAndType(Uri.parse("mailto:"), "text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        emailIntent.setType("message/rfc822");
        startActivity(Intent.createChooser(emailIntent, "send email"));

    }

    private void confirmDeleteAlert(final int travelID) {

        new AlertDialog.Builder(requireContext())
                .setTitle("Delete travel")
                .setMessage("Do you really want to delete this travel?")
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteTravel(travelID);
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    private void deleteTravel(int travelID) {

        Call<RestApiResponse> call = apiInterface.delete_travel(travelID);
        call.enqueue(new Callback<RestApiResponse>() {
            @Override
            public void onResponse(Call<RestApiResponse> call, Response<RestApiResponse> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    Toast.makeText(requireContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    NavHostFragment.findNavController(ShowSelectedTravelFragment.this)
                            .navigate(R.id.action_nav_show_single_travel_to_nav_user_history);
                }
            }

            @Override
            public void onFailure(Call<RestApiResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        doSomething = (DoSomething) context;
    }
}
