package com.example.nimsmit.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.nimsmit.ProfileActivity;
import com.example.nimsmit.R;
import com.example.nimsmit.model.ApiClient;
import com.example.nimsmit.model.ApiInterface;
import com.example.nimsmit.model.Helper;
import com.example.nimsmit.model.Login;
import com.example.nimsmit.model.RestApiResponse;
import com.example.nimsmit.model.Token;
import com.example.nimsmit.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * this class is the login fragment and contains method to log the user in.
 */

public class LoginUserFragment extends Fragment {


    private EditText login_email, login_password;
    private Button login;
    private String email, password;
    private Token token;
    private User data_user;
    private Login credentials;
    private ApiInterface apiInterface;

    public static final String SHARED_PREFS = "sharedprefs";
    public static final String EMAIL = "email";


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_login_user, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);

    }


    /**
     * this method initialise the view with all the components
     * @param view the created view
     */
    private void init(View view) {

        login_email = view.findViewById(R.id.login_email);
        login_password = view.findViewById(R.id.login_password);
        login = view.findViewById(R.id.login);

        // set api interface
        apiInterface = ApiClient.getClient();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIn();
            }
        });
        loadEmail();
        // password textview request focus if email not null
        if (email != null){
            login_password.requestFocus();
        }
        updateView();

    }


    /**
     * this method perform the log in
     */
    public void logIn() {

        email = login_email.getText().toString();
        password = login_password.getText().toString();

        if (!Helper.isEmpty(login_email) && !Helper.isEmpty(login_password)){
            credentials = new Login(email, password);
        }
        Call<RestApiResponse> call = apiInterface.login(credentials);
        call.enqueue(new Callback<RestApiResponse>() {
            @Override
            public void onResponse(Call<RestApiResponse> call, Response<RestApiResponse> response) {
                if(response.isSuccessful()){
                    assert response.body() != null;
                    token = new Token(response.body().getToken());
                    // validate token
                    validate_token(token);
                }
                else {
                    Toast.makeText(getActivity(), "Email or Password not correct", Toast.LENGTH_LONG).show();
                }
            }


            @Override
            public void onFailure(Call<RestApiResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }


    /**
     * this method perform the token validation, before the user is logged
     * @param token the token that require to be validate
     */
    private void validate_token(final Token token) {

        Call<RestApiResponse> call1 = apiInterface.validate_token(token);
        call1.enqueue(new Callback<RestApiResponse>() {
            @Override
            public void onResponse(Call<RestApiResponse> call, Response<RestApiResponse> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    assert response.body() != null;
                    data_user = response.body().getUser();
                    data_user.setToken(token.getToken());
                    saveEmail();
                    profile();
                }
                else {
                    Toast.makeText(getActivity(), "Access Denied!", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<RestApiResponse> call, Throwable t) {

            }
        });

    }


    /**
     * this method save email into preference so that the user need to pass his email once
     */
    private void saveEmail() {

        SharedPreferences emailDetail = requireContext().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = emailDetail.edit();
        editor.putString(EMAIL, email);
        editor.apply();

    }


    /**
     * this method load the saved email
     */
    private void loadEmail(){

        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        // the second parameter is the default email when no email is save
        email = sharedPreferences.getString(EMAIL, "");

    }


    /**
     * this method update the email view with the saved email
     */
    private void updateView(){
        login_email.setText(email);
    }


    /**
     * this method open the profile activity and send user data
     */
    private void profile() {

        Intent profile = new Intent(getActivity(), ProfileActivity.class);
        profile.putExtra("data_user",  data_user);
        startActivity(profile);
        requireActivity().finish();

    }




}
