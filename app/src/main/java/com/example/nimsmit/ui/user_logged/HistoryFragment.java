package com.example.nimsmit.ui.user_logged;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nimsmit.R;
import com.example.nimsmit.model.ApiClient;
import com.example.nimsmit.model.ApiInterface;
import com.example.nimsmit.model.DoSomething;
import com.example.nimsmit.model.RVAdapter;
import com.example.nimsmit.model.RestApiResponse;
import com.example.nimsmit.model.Travel;
import com.example.nimsmit.model.User;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryFragment extends Fragment implements RVAdapter.TravelAdapterListener {


    private List<Travel> travelsList;
    private ApiInterface apiInterface;
    private RVAdapter rvAdapter;
    private TextView no_travel_found;
    private DoSomething doSomething;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_show_travels, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        // populate travelsList
        getHistory();
    }

    private void init(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        travelsList = new ArrayList<>();
        no_travel_found = view.findViewById(R.id.no_travel_found);
        rvAdapter = new RVAdapter(travelsList, this);
        recyclerView.setAdapter(rvAdapter);
        apiInterface = ApiClient.getClient();
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {

        inflater.inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) requireActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_action_search).getActionView();
        assert searchManager != null;
        searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                rvAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // filter recycler view when text is changed
                rvAdapter.getFilter().filter(newText);
                return false;
            }
        });

    }



    private void getHistory() {

        User user_data = doSomething.getUser();
        int id = user_data.getUserID();

        Call<RestApiResponse> call = apiInterface.history(id);

        call.enqueue(new Callback<RestApiResponse>() {
            @Override
            public void onResponse(Call<RestApiResponse> call, Response<RestApiResponse> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    List<Travel> travels = response.body().getTravels();
                    for (int i = 0; i < travels.size(); i++){
                        String departure = travels.get(i).getDeparture();
                        String arrival = travels.get(i).getArrival();
                        String departure_date = travels.get(i).getDeparture_date();
                        String departure_hour = travels.get(i).getDeparture_hour();
                        String arrival_date = travels.get(i).getArrival_date();
                        String arrival_hour = travels.get(i).getArrival_hour();
                        int userID = travels.get(i).getUserID();
                        int id = travels.get(i).getId();
                        double weight = travels.get(i).getWeight();
                        double price = travels.get(i).getPrice();
                        String commentar = travels.get(i).getCommentar();

                        travelsList.add(new Travel(id, userID, departure, arrival, departure_date, departure_hour, arrival_date, arrival_hour, weight, price, commentar));
                    }
                    rvAdapter.notifyDataSetChanged();

                }
                else {
                    no_travel_found.setText("no travel found!");
                }
            }

            @Override
            public void onFailure(Call<RestApiResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onTravelSelected(Travel travel) {

        Bundle selectedTravel = new Bundle();
        selectedTravel.putSerializable("travel", travel);
        selectedTravel.putBoolean("delete", true);

        NavHostFragment.findNavController(this)
                .navigate(R.id.action_HistoryFragment_to_ShowSingleTravelFragment, selectedTravel);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        doSomething = (DoSomething) context;
    }
}
